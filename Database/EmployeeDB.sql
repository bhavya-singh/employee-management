CREATE TABLE `Employee` (
	`emp_id` INT(6) NOT NULL AUTO_INCREMENT,
	`first_name` varchar(20) NOT NULL,
	`middle_name` varchar(20),
	`last_name` varchar(20) NOT NULL,
	`last_updated` TIMESTAMP NOT NULL,
	`updated_by_id` INT(6) NOT NULL,
	`gender` varchar(10) NOT NULL,
	`DOB` DATE NOT NULL,
	`DOJ` DATE NOT NULL,
	`p_contact` INT(10) NOT NULL UNIQUE,
	`s_contact` INT(10) UNIQUE,
	`email_personal` varchar(50) NOT NULL UNIQUE,
	`email_company` varchar(50) NOT NULL UNIQUE,
	`desig_id` INT(10) NOT NULL,
	`dept_id` varchar(10) NOT NULL,
	`manager_id` INT(6) NOT NULL,
	`proj_id` INT(10) NOT NULL,
	`salary` FLOAT NOT NULL,
	`experience` INT NOT NULL,
	`acct_no` varchar(18) NOT NULL UNIQUE,
	`bank_name` varchar(20) NOT NULL,
	`aadhaar_no` INT(12) NOT NULL UNIQUE,
	`pan_no` varchar(10) NOT NULL UNIQUE,
	`p_address_line_1` varchar(50) NOT NULL,
	`p_address_line_2` varchar(50),
	`p_pincode` INT(6) NOT NULL,
	`p_city` varchar(30) NOT NULL,
	`p_state` varchar(30) NOT NULL,
	`p_country` varchar(30) NOT NULL,
	`s_address_line_1` varchar(50) NOT NULL,
	`s_address_line_2` varchar(50),
	`s_pincode` INT(6) NOT NULL,
	`s_city` varchar(30) NOT NULL,
	`s_state` varchar(30) NOT NULL,
	`s_country` varchar(30) NOT NULL,
	`employee_status` BOOLEAN NOT NULL,
	PRIMARY KEY (`emp_id`)
);

CREATE TABLE `Department` (
	`dept_id` varchar(10) NOT NULL,
	`dept_name` varchar(20) NOT NULL,
	`location` varchar(20) NOT NULL,
	`last_updated` TIMESTAMP NOT NULL,
	`updated_by_id` INT(6) NOT NULL,
	PRIMARY KEY (`dept_id`)
);

CREATE TABLE `Project` (
	`proj_id` INT(10) NOT NULL AUTO_INCREMENT,
	`proj_name` varchar(50) NOT NULL UNIQUE,
	`start_date` DATE NOT NULL,
	`end_date` DATE NOT NULL,
	`last_updated` TIMESTAMP NOT NULL,
	`updated_by_id` INT(6) NOT NULL,
	PRIMARY KEY (`proj_id`)
);

CREATE TABLE `Designation` (
	`desig_id` INT(10) NOT NULL AUTO_INCREMENT,
	`desig_name` varchar(50) NOT NULL UNIQUE,
	`desig_description` varchar(100),
	`last_updated` TIMESTAMP NOT NULL,
	`updated_by_id` INT(6) NOT NULL,
	PRIMARY KEY (`desig_id`)
);

CREATE TABLE `Intern_Activity` (
	`activity_id` INT(20) NOT NULL AUTO_INCREMENT UNIQUE,
	`emp_id` INT(6) NOT NULL,
	`last_updated` TIMESTAMP NOT NULL,
	`activity_date` DATE NOT NULL,
	`activity_type_id` INT(20) NOT NULL,
	`start_time` TIME NOT NULL,
	`end_time` TIME NOT NULL,
	`status` BOOLEAN NOT NULL DEFAULT '0',
	`summary` varchar(100),
	`comments` varchar(100),
	PRIMARY KEY (`activity_id`)
);

CREATE TABLE `Activity_Category` (
	`meeting` varchar(50) NOT NULL,
	`informal_interaction` varchar(50) NOT NULL,
	`learning_session` VARCHAR(50) NOT NULL,
	`project_development` varchar(50) NOT NULL,
	`IT_ticket` varchar(50) NOT NULL,
	`HR_query` varchar(50) NOT NULL,
	`activity_type_id` INT(20) NOT NULL
);

CREATE TABLE `Login` (
	`email_id` VARCHAR(50) NOT NULL UNIQUE,
	`password` varchar(20) NOT NULL AUTO_INCREMENT UNIQUE,
	`role` VARCHAR(20) NOT NULL AUTO_INCREMENT,
	`emp_id` INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`emp_id`)
);

ALTER TABLE `Employee` ADD CONSTRAINT `Employee_fk0` FOREIGN KEY (`desig_id`) REFERENCES `Designation`(`desig_id`);

ALTER TABLE `Employee` ADD CONSTRAINT `Employee_fk1` FOREIGN KEY (`dept_id`) REFERENCES `Department`(`dept_id`);

ALTER TABLE `Employee` ADD CONSTRAINT `Employee_fk2` FOREIGN KEY (`proj_id`) REFERENCES `Project`(`proj_id`);

ALTER TABLE `Intern_Activity` ADD CONSTRAINT `Intern_Activity_fk0` FOREIGN KEY (`emp_id`) REFERENCES `Employee`(`emp_id`);

ALTER TABLE `Activity_Category` ADD CONSTRAINT `Activity_Category_fk0` FOREIGN KEY (`activity_type_id`) REFERENCES `Intern_Activity`(`activity_type_id`);

ALTER TABLE `Login` ADD CONSTRAINT `Login_fk0` FOREIGN KEY (`emp_id`) REFERENCES `Employee`(`emp_id`);

