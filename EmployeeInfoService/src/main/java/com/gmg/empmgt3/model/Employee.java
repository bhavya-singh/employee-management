package com.gmg.empmgt.model;

import java.io.Serializable;
import java.util.Date;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.envers.Audited;

@Entity	// EMPLOYEE table created
@DynamicUpdate
@Audited
public class Employee extends AuditModel<String> implements Serializable
{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="emp_id", updatable=false, nullable=false)
	private Integer empId;
	
	@Column(name="pwd", nullable=false)
	private String password;
	
	@Column(name="version_no", nullable=false, columnDefinition="bigint default 1")
	private Long version;

	@Column(name="dept_id", nullable=false)
	private int deptId;
	
	@Column(name="desig_id", nullable=false)
	private int desigId;
		
	@Column(name="first_name", nullable=false)
	private String firstName;
	
	@Column(name="middle_name", nullable=true)
	private String middleName;
	
	@Column(name="last_name", nullable=false)
	private String lastName;
	
	@Column(name="c_email", nullable=false)
	private String companyEmail;
	
	@Column(name="p_email", nullable=true)
	private String personalEmail;
	
	@Column(name="mgr_id", nullable=false)
	private int mgrId;
	
	@Column(name="is_deleted", columnDefinition="boolean default false")
	private boolean isDeleted;
	
	@Column(name="gender", nullable=false)
	private char gender;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dob", nullable=false)
	private Date dob;
	
	@Temporal(TemporalType.DATE)
	@Column(name="doj", nullable=false)
	private Date doj;
	
	@Column(name="pri_contact", nullable=false)
	private Long primaryContact;
	
	@Column(name="sec_contact", nullable=true)
	private Long secondaryContact;
			
	@Column(name="project_id", nullable=false)	
	private int projectId;
	
	@Column(name="salary", nullable=false)
	private double salary;
	
	@Column(name="exp", nullable=false)
	private int experience;
	
	@Column(name="acct_no", nullable=false)
	private Long accountNo;
	
	@Column(name="bank_name", nullable=false)
	private String bankName;
	
	@Column(name="aadhaar_no", nullable=false)
	private Long aadhaarNo;
	
	@Column(name="pan_no", nullable=false)
	private Long panNo;
	
	@Column(name="p_add_line1", nullable=false)
	private String pAddressLine1;
	
	@Column(name="p_add_line2", nullable=false)
	private String pAddressLine2;
	
	@Column(name="p_pin_code", nullable=false)
	private int pPinCode;
	
	@Column(name="p_city", nullable=false)
	private String pCity;
	
	@Column(name="p_state", nullable=false)
	private String pState;
	
	@Column(name="p_country", nullable=false)
	private String pCountry;
	
	@Column(name="s_add_line1", nullable=true)
	private String sAddressLine1;
	
	@Column(name="s_add_line2", nullable=true)
	private String sAddressLine2;
	
	@Column(name="s_pin_code", nullable=true, columnDefinition="int default 0")
	private int sPinCode;
	
	@Column(name="s_city", nullable=true)
	private String sCity;
	
	@Column(name="s_state", nullable=true)
	private String sState;
	
	@Column(name="s_country", nullable=true)
	private String sCountry;

	
	// Getters and Setters

	
	public Integer getEmpId() {
		return empId;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public int getDesigId() {
		return desigId;
	}

	public void setDesigId(int desigId) {
		this.desigId = desigId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyEmail() {
		return companyEmail;
	}

	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}

	public String getPersonalEmail() {
		return personalEmail;
	}

	public void setPersonalEmail(String personalEmail) {
		this.personalEmail = personalEmail;
	}

	public int getMgrId() {
		return mgrId;
	}

	public void setMgrId(int mgrId) {
		this.mgrId = mgrId;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getDoj() {
		return doj;
	}

	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public Long getPrimaryContact() {
		return primaryContact;
	}

	public void setPrimaryContact(Long primaryContact) {
		this.primaryContact = primaryContact;
	}

	public Long getSecondaryContact() {
		return secondaryContact;
	}

	public void setSecondaryContact(Long secondaryContact) {
		this.secondaryContact = secondaryContact;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public int getExperience() {
		return experience;
	}

	public void setExperience(int experience) {
		this.experience = experience;
	}

	public Long getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(Long accountNo) {
		this.accountNo = accountNo;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Long getAadhaarNo() {
		return aadhaarNo;
	}

	public void setAadhaarNo(Long aadhaarNo) {
		this.aadhaarNo = aadhaarNo;
	}

	public Long getPanNo() {
		return panNo;
	}

	public void setPanNo(Long panNo) {
		this.panNo = panNo;
	}

	public String getpAddressLine1() {
		return pAddressLine1;
	}

	public void setpAddressLine1(String pAddressLine1) {
		this.pAddressLine1 = pAddressLine1;
	}

	public String getpAddressLine2() {
		return pAddressLine2;
	}

	public void setpAddressLine2(String pAddressLine2) {
		this.pAddressLine2 = pAddressLine2;
	}

	public int getpPinCode() {
		return pPinCode;
	}

	public void setpPinCode(int pPinCode) {
		this.pPinCode = pPinCode;
	}

	public String getpCity() {
		return pCity;
	}

	public void setpCity(String pCity) {
		this.pCity = pCity;
	}

	public String getpState() {
		return pState;
	}

	public void setpState(String pState) {
		this.pState = pState;
	}

	public String getpCountry() {
		return pCountry;
	}

	public void setpCountry(String pCountry) {
		this.pCountry = pCountry;
	}

	public String getsAddressLine1() {
		return sAddressLine1;
	}

	public void setsAddressLine1(String sAddressLine1) {
		this.sAddressLine1 = sAddressLine1;
	}

	public String getsAddressLine2() {
		return sAddressLine2;
	}

	public void setsAddressLine2(String sAddressLine2) {
		this.sAddressLine2 = sAddressLine2;
	}

	public int getsPinCode() {
		return sPinCode;
	}

	public void setsPinCode(int sPinCode) {
		this.sPinCode = sPinCode;
	}

	public String getsCity() {
		return sCity;
	}

	public void setsCity(String sCity) {
		this.sCity = sCity;
	}

	public String getsState() {
		return sState;
	}

	public void setsState(String sState) {
		this.sState = sState;
	}

	public String getsCountry() {
		return sCountry;
	}

	public void setsCountry(String sCountry) {
		this.sCountry = sCountry;
	}
		
}