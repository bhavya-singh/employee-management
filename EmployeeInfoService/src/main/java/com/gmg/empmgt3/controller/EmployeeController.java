package com.gmg.empmgt.controller;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Objects;
//import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gmg.empmgt.dao.EmpRepo;
//import com.gmg.empmgt.dao.HistoryRepo;
import com.gmg.empmgt.model.Employee;

// @Component + @ResponseBody
// marks EmployeeController as a request handler
// converts response to JSON
@RestController
public class EmployeeController {
	
	// injects an instance of EmpRepo here
	@Autowired
	EmpRepo empRepo;
	
	// DEFAULT welcome page
	@RequestMapping("/")
	public String home()
	{
		return "<h1> <center> Welcome to Employee Management </center> </h1>";
	}
	
	// displays ALL the current employees alphabetically
	@GetMapping("/employees")
	public List<Employee> getAllEmp()
	{
		// native query to display only current employees alphabetically
		return empRepo.findByMyQuery();	
	}
	
	// displays a particular employee by their empId
	// displays 'Could not find employee with ID : ###' if empId doesn't exist
	@GetMapping("/employee/{empId}")
	public Employee getEmp(@PathVariable int empId)
	{
		return empRepo.findById(empId).orElseThrow(()-> new EmployeeNotFoundException(empId));
	}

	// adds an employee to the database
	@PostMapping(path="/add", consumes= {"application/json"})
	public String addEmp(@RequestBody Employee emp)
	{
		int[] statusArr = new int[3];
		statusArr[0] = validMgrId(emp.getMgrId());
		statusArr[1] = validDeptId(emp.getDeptId());
		statusArr[2] = validDesigId(emp.getDesigId());
		
		if(statusArr[0]==2 && statusArr[1]==1 && statusArr[2]==1) 
		{
			empRepo.save(emp);
			return "Employee: "+emp.getEmpId()+" added successfully!";
		}
		else if(statusArr[2]==1)
		{
			if(statusArr[0]==1 && statusArr[1]==2) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any current manager\n "
					  +"Department ID: "+emp.getDeptId()+" doesn't exist\n"
					  +"Designation ID: "+emp.getDesigId()+" is correct.";
			}
			else if(statusArr[0]==3 && statusArr[1]==2) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any employee\n"
					  +"Department ID: "+emp.getDeptId()+" doesn't exist\n"
					  +"Designation ID: "+emp.getDesigId()+" is correct.";
			}
			else if(statusArr[0]==3 && statusArr[1]==1) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any employee\n"
					  +"Department ID: "+emp.getDeptId()+" is correct\n"
					  +"Designation ID: "+emp.getDesigId()+" is correct.";
			}
			else if(statusArr[0]==1 && statusArr[1]==1) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any current manager\n"
						+"Department ID: "+emp.getDeptId()+" is correct\n"
						+"Designation ID: "+emp.getDesigId()+" is correct.";
			}
			else if(statusArr[0]==2 && statusArr[1]==2) {
				return "Manager ID: "+emp.getMgrId()+" is correct\n"
						+"Department ID: "+emp.getDeptId()+" doesn't exist\n"
						+"Designation ID: "+emp.getDesigId()+" is correct.";
			}
		}
		else if(statusArr[2]==2)
		{
			if(statusArr[0]==1 && statusArr[1]==2) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any current manager\n "
						+"Department ID: "+emp.getDeptId()+" doesn't exist\n"
						+"Designation ID: "+emp.getDesigId()+" doesn't exist.";
			}
			else if(statusArr[0]==3 && statusArr[1]==2) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any employee\n"
						+"Department ID: "+emp.getDeptId()+" doesn't exist\n"
						+"Designation ID: "+emp.getDesigId()+" doesn't exist.";
			}
			else if(statusArr[0]==3 && statusArr[1]==1) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any employee\n"
						+"Department ID: "+emp.getDeptId()+" is correct\n"
						+"Designation ID: "+emp.getDesigId()+" doesn't exist.";
			}
			else if(statusArr[0]==1 && statusArr[1]==1) {
				return "Manager ID: "+emp.getMgrId()+" doesn't match any current manager\n"
						+"Department ID: "+emp.getDeptId()+" is correct\n"
						+"Designation ID: "+emp.getDesigId()+" doesn't exist.";
			}
			else if(statusArr[0]==2 && statusArr[1]==2) {
				return "Manager ID: "+emp.getMgrId()+" is correct\n"
						+"Department ID: "+emp.getDeptId()+" doesn't exist\n"
						+"Designation ID: "+emp.getDesigId()+" doesn't exist.";
			}
			else if(statusArr[0]==2 && statusArr[1]==1) {
				return "Manager ID: "+emp.getMgrId()+" is correct\n"
						+"Department ID: "+emp.getDeptId()+" is correct\n"
						+"Designation ID: "+emp.getDesigId()+" doesn't exist.";
			}
		}
		return null;
	}
		
	public int validMgrId(int passedMgrId)
	{
		// passedMgrId exists in database
        if (empRepo.existsById(passedMgrId)==true)
        {
        	// CASE 1 - MANAGER ID EXISTS BUT HAS BEEN DELETED
            if(empRepo.getEmpById(passedMgrId).isDeleted()==true) {return 1;}
	        // CASE 2 - VALID MANAGER ID
	        else {return 2;}  
        }
        // CASE 3 - INVALID MANAGER EMPID
        return 3;
	}

	// valid department IDs
	public int[] deptArr = {10,20,30,40,50,60};
	
	// validating entered department ID
	public int validDeptId(int passedDeptId)
	{
		// CASE 1 - PASSED DEPT ID exists in database
        if (contains(deptArr, passedDeptId)) {return 1;}
        // CASE 2 - PASSED DEPT ID doesn't exist in database
        else {return 2;}  
	}

	// checking if the passed value is in array
	public boolean contains(int[] array, int passedVal)
	{
		boolean result = false;
		for(int var: array)
		{
			if (passedVal == var)
			{
				result = true;
			}
		}
		return result;
	}
	
	// valid designation IDs 
	public int[] desigArr = {1,2,3,4,5,6,7,8,9,10};
	
	// validating entered designation ID	
	public int validDesigId(int passedDesigId)
	{
		// CASE 1 - PASSED DEPT ID exists in database
        if (contains(desigArr, passedDesigId)) {return 1;}
        // CASE 2 - PASSED DEPT ID doesn't exist in database
        else {return 2;}  
	}	
	
	// updates the modifiable details of an employee
	@PutMapping(path="/update/{empId}", consumes= {"application/json"})
	public String updateEmp(@RequestBody Employee passedEmp, @PathVariable int empId)
	{
		// CASE 1 - VALID EMPID IS PASSED
	    try
	    {
            // unproxy the hibernate-proxy class object
            // cast as Employee type
            Employee empById = (Employee) Hibernate.unproxy(empRepo.getEmpById(empId));
            Long oldVersion = empById.getVersion();
            
            
            // updates details if employee is currently with company
            // && ( passed empID == updating empID )
            if (empById.isDeleted()==false && empById.getEmpId()==passedEmp.getEmpId())
            {
                int changedFields = 0;
            	for (Field field : empById.getClass().getDeclaredFields())
                {
        			// to access private variables of the Entity class
                    field.setAccessible(true);
                    try
                    {
                    	// getting the values of the current field in old and new object
                        Object oldValue = field.get(empById);        
                        Object newValue = field.get(passedEmp);
                        
                        // if the value needs to be updated
                        if (!Objects.equals(oldValue, newValue)) 
                        {
                                field.set(empById, newValue);
                                changedFields++;
                        }
                    }
                    catch(IllegalAccessException e)
                    {
                        return "Fields cannot be accessed";
                    }
                    catch(IllegalArgumentException e)
                    {
                        return "Wrong Argument \n"+e.getStackTrace();
                    }
                }
            	
                // if values have been changed
                if(changedFields > 0)
                {
                	empById.setVersion(++oldVersion);
	                empRepo.save(empById);
	                return "Updated Employee : "+empId;
                }
            }
        
            // CASE 2 - EMPLOYEE HAS BEEN DELETED
            else if(empById.isDeleted()==true)
            {
                return "Employee : "+empId+ " is no longer with the company.";
            }
    	}
	  
	    //  CASE 3 - INVALID EMPID IS PASSED
	    catch(EntityNotFoundException e)
	    {
	            return "Could not find employee with ID : " + empId;
	    }
	    return null;    
	}
	
	// deletes an employee by setting the employee's 'isDeleted' field to true 
	@PutMapping(path="/delete/{empId}")
	public String deleteEmp(@PathVariable int empId)
	{
		// if valid empId is passed
		try 
		{	
			Employee emp = empRepo.getEmpById(empId);
			
			// if the employee has already been deleted
			if(emp.isDeleted()==true)
			{
				return "Employee : "+empId+ " has already been deleted.";
			}
			
			// to delete a current employee
			else if (emp.isDeleted()==false)
			{
				emp.setDeleted(true);
				empRepo.save(emp);
				return "Deleted Employee : "+empId;
			}
		}
		
		// invalid empId entered
		catch(EntityNotFoundException e)
		{
			return "Could not find employee with ID : " + empId;
		}
		return null;
	}
}
