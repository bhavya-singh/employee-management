package com.gmg.empmgt.controller;

// exception raised when Employee doesn't exist in database
public class EmployeeNotFoundException extends RuntimeException 
{
	private static final long serialVersionUID = 1L;

	EmployeeNotFoundException(int empId)
	{
		// Message returned
		super("Could not find employee with ID : " + empId);
	}
}
