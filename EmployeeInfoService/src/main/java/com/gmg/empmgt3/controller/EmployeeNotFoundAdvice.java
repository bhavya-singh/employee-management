package com.gmg.empmgt.controller;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

// Global Exception Handler
// global code that can be applied to raise an exception when Employee isn't found in database
@ControllerAdvice
public class EmployeeNotFoundAdvice {

	  @ResponseBody
	  // takes the exception class as argument
	  @ExceptionHandler(EmployeeNotFoundException.class)
	  @ResponseStatus(HttpStatus.NOT_FOUND)
	  String employeeNotFoundHandler(EmployeeNotFoundException ex) 
	  {
		  return ex.getMessage();
	  }
}
	
