package com.gmg.empmgt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

//import com.gmg.empmgt.model.Department;
import com.gmg.empmgt.model.Employee;

//handles database interactions
public interface EmpRepo extends JpaRepository<Employee, Integer> 	
{
	// method to retrieve entries with isDeleted = false, ordered alphabetically
	@Query(value="SELECT * FROM employee e WHERE is_deleted = false or is_deleted IS NULL ORDER BY first_name", nativeQuery=true)
	List<Employee> findByMyQuery();
		
	default Employee getEmpById(int empId) {
		return getOne(empId);
	}
}