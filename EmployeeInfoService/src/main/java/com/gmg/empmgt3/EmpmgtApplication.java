package com.gmg.empmgt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/* 
 * combines @Configuration (to enable Java-based configuration),
 * @ComponentScan (to enable component scanning) &
 * @EnableAutoConfiguration (to enable Spring Boot's auto-configuration feature).
*/
@SpringBootApplication											
public class EmpmgtApplication {

	public static void main(String[] args) 
	{
		// Bootstraps a spring application as a stand-alone application,
		// creates ApplicationContext instance, loads beans and runs embedded Tomcat server.
		SpringApplication.run(EmpmgtApplication.class, args);	
	}

}
