# Employee Management

> **‘Employee Management System’ aims at providing the company and its employees a flexible and easy to use web application. It entails the following: searching, adding and updating the employee details along with viewing and adding Activities.**

Below are the Architecture, Wireframes and DB Designs explained in detail.
The documentation seeks to explain the following aspects of the project:
-   entire working
-   technologies used
-   functionality


##### <ins>**Technical Requirements:**</ins>

<ins>_UI_</ins>: Node JS, Angular CLI and Visual Studio code.  
<ins>_DB_</ins>: MySql Database.  
<ins>_Microservices_</ins>: Spring tool suite, Tomcat Server, Java 11 , Hibernate.

##### <ins>**Responsibilities:**</ins>
<ins>_UI_</ins>: Avi Jain  
<ins>_DB_</ins>: M. SreeVidya  
<ins>_Microservices_</ins>: Bhavya Singh, S. Yashasvi  


### Architecture
---
![](https://gitlab.com/bhavya-singh/employee-management/-/raw/master/images/Employee_Architecture.png)


### Wireframes
---

[Employee Management UI Wireframes PDF](images/Employee.pdf)


### Microservices
---

<ins>_Login microservice :_</ins>  
Lets a user enter the Employee Management System by verifying their credentials and
assigning the level of access corresponding to their role.  

<ins>_Employee microservice :_</ins>  

Serves requirements such as search, add, delete and update of information considering various access level restrictions.

- 'Search' method finds the Employee details matching the user query. Returns back a list of relevant employees. Upon selection of a particular employee, displays the selected employee's details.  
- 'Add' method takes up the inputs entered by the employee and stores it into the respective table fields in database.  
- 'Delete' method finds the Employee to be deleted using their employee ID and changes their employment status in the database.  
- 'Update' method displays the editable fields in correspondence to the user's access level. Once changes are made, sends back the entered information to the respective fields in the database.  

<ins>_Activity microservice :_</ins>  

This takes up the details of an activity performed by an employee.  

- 'Activity Log' method takes up the details of an activity performed by an employee.
- 'View Activity' method displays a list of activities performed by an employee. Allows the displayed list to be sorted and filtered by date, employee ID or category of the activity.


### Database Design
---

![Database Design](https://gitlab.com/bhavya-singh/employee-management/-/raw/master/images/dbdesign.png)

<ins>_Employee Table :_</ins>  
It consists of all the personal (name,contacts,addresses etc) and company
specific(empId,managerId,projectId etc) details related to an employee.  
<ins>_Designation Table :_</ins>  
It contains the information regarding the employee's designation and its 
related fields.  
<ins>_Department Table :_</ins>  
It provides the details of the Department for which the employee is
working like dept_id etc.  
<ins>_Project Table :_</ins>  
It has details regarding the projects on which the employee has worked or is
presently working on like the proj_id etc.  
<ins>_Activity Category Table :_</ins>  
It keeps the track of all the activities the employee was involved in
like meetings, informal discussions etc.  
<ins>_Intern Activity Table :_</ins>  
It keeps the info regarding activities and the details related to intern
activities.  
<ins>_Login Table :_</ins>  
It maintains the email_ids, passwords and related credential information to
provide access to employees.