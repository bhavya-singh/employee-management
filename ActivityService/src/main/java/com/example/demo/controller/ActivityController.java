package com.example.demo.controller;




import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.model.Activity;
import com.example.demo.model.Emp;
import com.example.demo.repository.ActivityRepo;

import com.example.demo.repository.EmpRepository;
import com.example.demo.exception.ActivityTypeNotFoundException;
import com.example.demo.exception.DateNotFoundException;
import com.example.demo.exception.EmployeeNotFoundException;



@RestController
@RequestMapping("/activity")
public class ActivityController {

	@Autowired
	ActivityRepo repo;
	@Autowired
	EmpRepository empRepo;
    @Autowired
    RestTemplate restTemplate;
	@GetMapping("/")
	private String home()
	{
		return "Works well";
	}
	
	//Adding activities
	@CrossOrigin(origins="http://localhost:4200")
	@PostMapping(path="/add")
	public Activity addActivity(@RequestBody Activity activity)
	{	
		repo.save(activity);
		return activity;
	}
	
	//Displays the activities of all the employees 
		@GetMapping(path="/viewAll",produces={"application/json"})
		public List<Activity> viewAllActivities()
		{
			return repo.findAll();
		}
		
	//Displays details of activities of an employee by giving emp_id
	@GetMapping(path="/viewById/{emp_id}",produces={"application/json"})
	public Optional<List<Activity>> viewActivityById(@PathVariable int emp_id)
	{
	  return Optional.of(repo.findByEmpId(emp_id).orElseThrow(()-> new EmployeeNotFoundException(emp_id)));
	}	
	
	//Displays details of activities of an employee on a particular date
	@GetMapping(path="/viewByDate/{emp_id}/{activity_date}",produces={"application/json"})
	public Optional<List<Activity>> viewActivityByDate(@PathVariable int emp_id,@PathVariable("activity_date") @DateTimeFormat(pattern = "yyyy-MM-dd")Date activity_date)
	{
	  return Optional.of(repo.findByDate(emp_id,activity_date).orElseThrow(()-> new DateNotFoundException(emp_id,activity_date)));	
	}
	
	//Displays details of activities of an employee of a particular type 
	@GetMapping(path="/viewByType/{emp_id}/{activity_type}",produces={"application/json"})
	public Optional<List<Activity>> viewActivityByActivityType(@PathVariable int emp_id,@PathVariable String activity_type)
	{
		return Optional.of(repo.findByActivityType(emp_id,activity_type).orElseThrow(()-> new ActivityTypeNotFoundException(emp_id,activity_type)));
	}
	
	
	//to add passwords to the emp table
	@PostMapping(path="/emp/add/{pwd}",produces={"application/json"})
    public String add(@PathVariable String pwd)
    {
		Emp emp = new Emp();
		emp.setPassword(pwd);
		empRepo.save(emp);
		return "User created";
    }
	
	//Method used to replicate update method of EmpInfoService
	//This logic has to be applied in update method if the password field is changed
	@PutMapping(path="/emp/add/{id}",produces= {"application/json"})
	public String update(@RequestBody Emp emp,@PathVariable int id)
    {
		if(id==emp.getId())
		{
		  Emp empById = empRepo.findById(id).orElseThrow(()-> new EmployeeNotFoundException(id));
		  if(!emp.getPassword().equals(empById.getPassword()))
		  {
		   String pwd=empById.getPassword();
		   empRepo.save(emp);
		   HttpEntity<Emp> entity = new HttpEntity<Emp>(emp);
		   return restTemplate.exchange(
		     "http://localhost:8300/login/updateByPassword/"+pwd+"/"+emp.getPassword(), HttpMethod.PUT, entity, String.class).getBody();	
		  }	
		  return "password not changed";
		}
		else
		{
	      return "The employee ids do not match";
		}
    } 
}
