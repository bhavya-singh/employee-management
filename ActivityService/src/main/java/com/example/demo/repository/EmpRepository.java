package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Emp;
import java.util.Optional;

public interface EmpRepository extends JpaRepository<Emp,Integer>{

	Optional<Emp> findById(int id);
}
