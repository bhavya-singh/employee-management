package com.example.demo.repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Activity;

public interface ActivityRepo extends JpaRepository<Activity,Integer> {
	//@Query(value="SELECT * FROM activity a WHERE a.emp_id=emp_id ", nativeQuery=true)
	Optional<List<Activity>> findByEmpId(int emp_id);
	
	
	/*@Query("select a from Article a where a.creationDateTime <= :creationDateTime")
    List<Article> findAllWithCreationDateTimeBefore(
      @Param("creationDateTime") Date creationDateTime);*/
	
	@Query(value="SELECT * FROM activity a WHERE a.emp_id=:emp_id AND a.activity_date = :activity_date", nativeQuery=true)
	Optional<List<Activity>> findByDate(@Param("emp_id")int emp_id,@Param("activity_date")Date activity_date);

	@Query(value="SELECT * FROM activity a WHERE a.emp_id=:emp_id and a.activity_type =:activity_type", nativeQuery=true)
	Optional<List<Activity>> findByActivityType(int emp_id,String activity_type);

	
}
