package com.example.demo.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@DynamicUpdate
public class Activity extends AuditModel<String>{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="activity_id", updatable=false, nullable=false)
	private Integer activityId;
	
	@Column(name="emp_id", nullable=false)
	//@Column(name="emp_id")
	private Integer empId;
	
	@Column(name="activity_type", columnDefinition = "varchar(255) default 'project_development'")
	private String activityType;
	
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="start_time", nullable=false)
	//private Date startTime;
	private String startTime;
	
	//@Temporal(TemporalType.TIMESTAMP)
	@Column(name="end_time")
	//private Date endTime;
	private String endTime;
	
	//@Column(name="status",columnDefinition="boolean default false" )
	//private boolean status;
	@Column(name="status" )
	private String status;
	
	@Column(name="summay")
	private String summary;
	
	@Column(name="comments")
	private String comments;
	
	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	//public Date getStartTime() {
	public String getStartTime() {
		return startTime;
	}

	//public void setStartTime(Date startTime) {
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	//public Date getEndTime() {
	public String getEndTime() {
	return endTime;
	}

	//public void setEndTime(Date endTime) {
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	//public boolean isStatus() {
	public String isStatus() {
		return status;
	}

	//public void setStatus(boolean status) {
	public void setStatus(String status) {
		this.status = status;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	


}
