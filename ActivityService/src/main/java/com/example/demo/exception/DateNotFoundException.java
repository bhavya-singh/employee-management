package com.example.demo.exception;

import java.util.Date;

public class DateNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DateNotFoundException(int emp_id,Date activity_date) {
		super("Activities on date : " +activity_date+" , of employee "+emp_id+" doesn't exist");

	}
}
