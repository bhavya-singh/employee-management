package com.example.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(EmployeeNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleEmployeeNotFoundException(EmployeeNotFoundException ex)
	{
		return ex.getMessage();
	}
	
	@ExceptionHandler(DateNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleDateNotFoundException(DateNotFoundException ex)
	{
		return ex.getMessage();
	}

	@ExceptionHandler(ActivityTypeNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String handleActivityTypeNotFoundException(ActivityTypeNotFoundException ex)
	{
		return ex.getMessage();
	}
	
}
