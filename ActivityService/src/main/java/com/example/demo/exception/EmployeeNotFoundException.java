package com.example.demo.exception;

public class EmployeeNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;


	public EmployeeNotFoundException(int emp_id) {
		super("Employee with employeeId : " +emp_id+" doesn't exist");
	}
}
