package com.example.demo.exception;

public class ActivityTypeNotFoundException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public ActivityTypeNotFoundException(int emp_id,String activity_type) {
		super("Activities of type :" +activity_type+" , of employee "+emp_id+" doesn't exist");

	}

}
